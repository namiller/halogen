#ifndef HALOGEN_HALOGEN_VARIABLES_H
#define HALOGEN_HALOGEN_VARIABLES_H

#include <vector>
#include <set>
#include <map>
#include "absl/memory/memory.h"

// Class definitions related to variable types.

namespace halogen {

// Super class that defines common interface for all variables.
class Variable {};

// Super class that defines the abstract super type for all data buffers.
class View : Variable {
 public:
  virtual int dimensions() = 0;
  virtual std::vector<int> extents() = 0;
};

class Input : public View {
 public:
  Input(int size) : size_(size) {}
  int dimensions() override {
    return 1;
  }
  std::vector<int> extents() override {
    return std::vector<int>({size_});
  }

 private:
  int size_;
};

class DiscreteView : public View {
 public:
  int dimensions() override {
    return 0;
  }
  std::vector<int> extents() override {
    return std::vector<int>();
  }
};

// A Placeholder variable. This is a transient object that represents a concept
// in the halogen language rather than an object.
class Placeholder : Variable {};

class Output : Placeholder {
 public:
  Output(const DiscreteView& view) : outputView_(view) {}

 private:
  const DiscreteView outputView_;
};

// A dimension variable is a variable that indexes into the dimensions of a
// coordinate space. For example standard dimension variables might be x, y, z.
class DimensionVariable : public Variable {
 public:
  DimensionVariable(int dimension) : dimension_(dimension) {}
  // Returns the index of the coordinate this variable represents.
  int dimension() const { return dimension_; }

 private:
  int dimension_;
};

static const DimensionVariable x(0);
static const DimensionVariable y(1);
static const DimensionVariable z(2);
static const DimensionVariable c(3);

}  // namespace halogen

#endif  // HALOGEN_HALOGEN_VARIABLES_H

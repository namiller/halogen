#include "halogen/variables.h"

namespace halogen {

template<typename T>
class DimensionExpression;

template <typename T>
class Expression {
 public:
  virtual T eval(const std::vector<T>& pt) const {
    return T();
  }
  DimensionExpression<T> operator ,(const Expression<T>& rhs) {
    return DimensionExpression<T>(*this), rhs;
  }
};

// A bundle of multiple expressions (1 per dimension).
template <typename T>
class DimensionExpression {
 public:
  std::vector<T> eval(const std::vector<T>& pt) const {
    std::vector<T> ret;
    for (const auto& expr : expressions_) {
      ret.push_back(expr.eval(pt));
    }
    return ret;
  }

  DimensionExpression(const Expression<T>& expression) {
    expressions_.push_back(expression);
  }

  DimensionExpression<T> operator ,(const Expression<T>& rhs) {
    expressions_.push_back(rhs);
    return *this;
  }

  DimensionExpression<T> operator,(const DimensionExpression<T>& rhs) {
    for (const auto& expr : rhs.expressions_) {
      expressions_.push_back(expr);
    }
    return *this;
  }

  int output_dimensions() const { return expressions_.size(); }

 protected:
  std::vector<Expression<T>> expressions_;
};


class RealExpression : Expression<float> {};

class RealDimensionExpression : DimensionExpression<float>{};

template <typename T>
class ConstantExpression : Expression<T>{
 public:
  ConstantExpression(T val) : val_(val) {}

  T eval(const std::vector<T>& pt) const override {
    return val_;
  }

 private:
  T val_;
};

ConstantExpression<float> Const(float f) {
  return ConstantExpression<float>(f);
}

ConstantExpression<int> Const(int f) {
  return ConstantExpression<int>(f);
}

template <typename T>
class IdentityExpression : Expression<T> {
 public:
  IdentityExpression(const DimensionVariable& dim) : dim_index_(dim.dimension()) {}
  T eval(const std::vector<T>& pt) const override {
    assert(dim_index_ < static_cast<int>(pt.size()) && dim_index_ >= 0);
    return pt[dim_index_];
  }

 private:
  int dim_index_;
};

IdentityExpression<float> Real(const DimensionVariable& dim) {
  return IdentityExpression<float>(dim);
}

IdentityExpression<int> Int(const DimensionVariable& dim) {
  return IdentityExpression<int>(dim);
}


}  // namespace halogen

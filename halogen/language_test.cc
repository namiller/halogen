#include "gtest/gtest.h"
#include "halogen/expressions.h"
#include "halogen/variables.h"

namespace halogen {

TEST(LanguageTest, ExpressionComposability) {
  RealExpression e1;
  RealExpression e2;
  RealExpression e3;
  auto f = (e1, e2, e3);
  EXPECT_EQ(f.output_dimensions(), 3);
}

}  // namespace halogen

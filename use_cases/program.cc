#include "types/variables.h"
#include "formats/layouts.h"
#include "formats/colors.h"
int main(int argc, char** argv) {

	auto input = halogen::Input(300*400*3);
	auto rgb = input(formats::planar(400, 300));
	auto reflect = rgb(400 - halogen::x, halogen::y, halogen::c);
	auto gray = reflect(formats::RGB2GRAY);
	auto scaled = gray(halogen::x*4.0, halogen::y*3.0);
	halogen::Output output1 = gray(halogen::x = [0, 100], halogen::y = [0, 100]);

	// Use output1.run(input) or output1.compile("file/path").
	return 0;
}

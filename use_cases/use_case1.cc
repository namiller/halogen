#include <set>
#include <utility>
#include <vector>

#include "types/ast1.h"
#include "types/ast2.h"
#include "gtest/gtest.h"

// This use case takes in a planar RGB image that is 640x480 and outputs an
// interleaved RGB image that is 10x10 cropped to the middle of the input.

/**
 * Source program:
 *
 * // Import the standard definitions for color space serializations
 * #import “colorspaces.h”
 *
 * // Alias the names for the dimension indexes for ease.
 * auto x = dim(0);
 * auto y = dim(1);
 * auto c = dim(2);
 *
 * auto height = InputParameter(“height”);
 * auto width = InputParameter(“width”);
 *
 * // RGB can be used in the input domain to define the 1D -> 3D pixel mapping
 * image = input(RGB(height, width));
 *
 * // RGB can be used in the output domain since it is invertible to define 3D
 * // -> 1D map.
 *  output(RGB(10,10)) = image(x = [10, 20], y = [30, 40], c);
 *  yield(output);
 **/

TEST(UseCase1, AST1) {
  ast::Ast1Sparse input;
  // All dimensionality is infered at compile time.
  input.dimensionality = 1;
  // input.values = /* some reader */;

  ast::Ast1Expr rgb;
  int d1 = 640;
  int d2 = 480;
  int d3 = 3;
  rgb.map = ast::RnRmFunction([d1,d2,d3](std::vector<float> indexes_in) {
    EXPECT_EQ(indexes_in.size(), 1u);
    int in = indexes_in[0];
    return std::vector<float>(
        {float(in % d1), float((in % (d1 * d2)) / d1), float(in / d3)});
  });
  rgb.in_dim = 1;
  rgb.out_dim = 3;

  ast::Ast1View rgb_view;
  rgb_view.expr = &rgb;
  rgb_view.source = &input;
  rgb_view.dimensionality = 3;


  ast::Ast1Discretization discretization;
  ast::Frange d1_range, d2_range, d3_range;
  d1_range.start = 10;
  d1_range.stop = 20;
  d2_range.start= 30;
  d2_range.stop = 40;
  d3_range.start = 0;
  d3_range.stop = 3;
  discretization.domains = {d1_range, d2_range, d3_range};
  ast::Ast1Yieldable output;
  output.domain = &discretization;
  output.data = &rgb_view;
  EXPECT_EQ(&output, &output); // Supress an unused warning.
}


TEST(UseCase1, AST2) {
  ast::Ast2Buffer input;

  // The dimensional data used to compute these functions during ast1 -> ast2.
  int d1 = 640;
  int d2 = 480;
  int d3 = 3;
  ast::Ast2Expr planar_to_interleaved;

  planar_to_interleaved.map = ast::ReorderMap([d1, d2, d3](int index) {
    int sz = d1 * d2;
    return ((index % sz) * d3) + (index / sz);
  });

  ast::Ast2View interleaved;
  interleaved.expr = &planar_to_interleaved;
  interleaved.source = &input;

  // The values of crop region would be expanded during desugaring from some
  // domain specification.
  std::vector<int> crop_region;
  for (int x = 0; x < d1; x++) {
    for (int y = 0; y < d2; y++) {
      for (int c = 0; c < d3; c++) {
        if (x >= 10 && x < 20 && y >= 30 && y < 40) {
          crop_region.push_back(x * d3 + y * d2 * d3 + c);
        }
      }
    }
  }

  ast::Ast2Output output;
  output.source = &interleaved;
  output.indexes = crop_region;
}


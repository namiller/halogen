#include <set>
#include <utility>
#include <vector>

#include "types/ast2.h"
#include "gtest/gtest.h"

/*
Source Program:

// This defines the RGB dimension parameterization for an interleaved 4 pixel
// aligned buffer.
class RGB : public DimP {
  Dim<3> map(Dim<1> input, Var height, Var width) {
    auto x = dim(0);
    auto y = dim(1);
    x.size = width;  // provide a hint for the way the x variable repeats.
    y.size = height; // .. for y.
    auto c = dim(2);
    // Define names for the variables used to show an example of a layout for
    // the indexes into the rolled out dimension
    c[0] = “R”;
    C[1] = “G”;
    C[2] = “B”;
    // Constructs a mapping based on an example. The ... indicates repetition in
    // that dimension.
    return by_example(“
      RGBRGBRGBRGB...
      ...“_e);
  }
};

auto x = dim(0);
auto y = dim(1);
auto c = dim(2);
// Note that rgb here implicitly gets expanded to rgb(dim(0), dim(1), dim(2)).
rgb = input(RGB(480, 640));

// Perform some pixelwise operations on planes of the image and recombine to
// perform  RGB->YUV colorspace transform.
yuv(x,y,_) = {
  .299*rgb(x,y,0) + rgb(x,y,1)*.587 + rgb(x,y,2)*.114,
  .492*(rgb(x,y,2) - (rgb(x,y,0)*.299 + rgb(x,y,1)*.587 + rgb(x,y,2) * .114)),
  .877*(rgb(x,y,0) - (rgb(x,y,0)*.299 + rgb(x,y,1)*.587 + rgb(x,y,2) * .114))
};

mirror = yuv(width - x, y, c);
x_scale = width / 100;
y_scale = height / 100;
scaled = yuv(x = [0, width, x_scale], y = [0, height, y_scale], c);
output(RGB(100, 100)) = yuv;
yield(output);
*/


// This takes in an interleaved RGB image that is 640x480 and outputs an
// interleaved YUV image that is downsampled to 100x100.
TEST(UseCase3, AST2) {
  ast::Ast2Buffer input;

  // The dimensional data used to compute these functions during ast1 -> ast2.
  int d1 = 640;
  int d2 = 480;
  int d3 = 3;

  ast::Ast2Expr interleaved_rgb_to_yuv;
  // This function will be generated based on the composition of the individual
  // dimensional maps, as well as the dimensional size data.
  interleaved_rgb_to_yuv.map = ast::WeightedMap([d1, d2, d3](int index) {
    std::set<std::pair<int, float>> values;

    int R = 3 * (index / 3);
    int G = 3 * (index / 3) + 1;
    int B = 3 * (index / 3) + 2;
    switch(index % 3) {
      case 0:  // Y
        values.insert(std::make_pair(R, .299));
        values.insert(std::make_pair(G, .687));
        values.insert(std::make_pair(B, .114));
        break;
      case 1:  // U
        values.insert(std::make_pair(R, -0.147));
        values.insert(std::make_pair(G, -0.289));
        values.insert(std::make_pair(B, .436));
        break;
      case 2:  // V
        values.insert(std::make_pair(R, .615));
        values.insert(std::make_pair(G, -0.515));
        values.insert(std::make_pair(B, -.100));
        break;
    }
    return values;
  });

  ast::Ast2Expr mirror;
  mirror.map = ast::ReorderMap([d1, d2, d3](int index) {
    int x = (index / d3) % d1;
    int y = index / (d3 * d1);
    int c = index % d3;

    int x_out = d1 - x;
    int y_out = y;
    int c_out = c;
    return y_out * d1 * d3 + x_out * d3 + c_out;
  });

  ast::Ast2View yuv;
  yuv.expr = &interleaved_rgb_to_yuv;
  yuv.source = &input;

  ast::Ast2View mirrored;
  mirrored.expr = &mirror;
  mirrored.source = &yuv;

  // The values of crop region would be expanded during desugaring from some
  // domain specification. In this case a strided domain.
  std::vector<int> crop_region;
  for (float x = 0; x < d1; x += (d1 / 100.0)) {
    for (float y = 0; y < d2; y += (d2 / 100.0)) {
      for (int c = 0; c < d3; c++) {
        crop_region.push_back(int(x) * d3 + int(y) * d2 * d3 + c);
      }
    }
  }

  ast::Ast2Output output;
  output.source = &mirrored;
  output.indexes = crop_region;
}


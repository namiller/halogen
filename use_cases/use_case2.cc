#include <set>
#include <functional>
#include <utility>
#include <vector>
#include <math.h>

#include "solver/solver.h"
#include "types/ast2.h"
#include "gtest/gtest.h"

// This case takes in a aligned grayscale image that is 640x480 and dewarps it
// and downsamples it to a 100x100 image.

/**
 * Source program:
 *
 * #import “colorspaces.h”
 * // Read in 64 byte line aligned greyscale image
 * image(x,y) = input(GREY_ALIGNED(64, 640, 480));
 * // Center the image for computational simplicity
 * centered(x, y) = image(x + 320, y + 240);
 *
 * // Remove some lens warping from the image
 * r = sqrt(x**2 + y**2)
 * safe_rat(v, r) { return r ? v / r : v; }
 * dewarped(x,y) = centered(x * 1.3 * safe_rat(x,r), y * 1.6 * safe_rat(x,r));
 * // Resample the image and write it to the output aligned to 16 bytes
 * sampled = dewared(x = [-320, 320], y = [-240, 240])
 * output(GREY_ALIGNED(16, 640, 480)) = sampled(x,y)
 **/

TEST(UseCase2, AST2) {
  ast::Ast2Buffer input;

  // The dimensional data used to compute these functions during ast1 -> ast2.
  int d1 = 640;
  int d2 = 480;
  int align = 64;
  ast::Ast2Expr aligned_to_packed;

  aligned_to_packed.map = ast::ReorderMap([d1, d2, align](int index) {
    int row_padding = (align - (d1 % align)) % align;
    return (index / d1) * row_padding + index;
  });


  ast::Ast2Expr center;
  center.map = ast::ReorderMap([d1, d2](int index) {
    // This dimension decomposition is performed during ast1 -> ast2.
    int x = index % d1;
    int y = index / d1;
    return (x + d1 /2) + (y + d2 / 2) * d1;
  });

  std::function<std::pair<float, float>(float, float)>
      user_fnct([](float x, float y) {
    float r = sqrt(x*x + y*y);
    return std::make_pair(x * 1.3 * (x / r), y * 1.6 * (y / r));
  });

  ast::Ast2Expr dewarp;
  dewarp.map = ast::ReorderMap([d1, d2, user_fnct](int index) {
    // TODO(namiller): This doesn't generalize in quite the right way.. Need to
    // think about how this works in these cases.
    int x = index % d1;
    int y = index / d1;
    auto loc = user_fnct(x, y);
    int x_out = loc.first;
    int y_out = loc.second;
    return x_out + y_out * d1;
  });

  ast::Ast2View packed;
  packed.expr = &aligned_to_packed;
  packed.source = &input;

  ast::Ast2View centered;
  centered.expr = &center;
  centered.source = &packed;

  ast::Ast2View dewarped;
  dewarped.expr = &dewarp;
  dewarped.source = &centered;

  // The values of crop region would be expanded during desugaring from some
  // domain specification.
  std::vector<int> crop_region;
  for (int x = -320; x < 320; x++) {
    for (int y = -240; y < 240; y++) {
      crop_region.push_back(y * d1 + x);
    }
  }

  ast::Ast2Output output;
  output.source = &dewarped;
  output.indexes = crop_region;
}


TEST(UseCase2, Solver) {
  int d1 = 640;
  int d2 = 480;

  std::vector<std::vector<float>> initial_known_points;
  for (float x = 0; x < d1; x++) {
    for (float y = 0; y < d2; y++) {
      initial_known_points.push_back({x,y});
    }
  }

  auto apply = [](std::vector<std::vector<float>> knowns, std::function<std::vector<float>(float,float)> permutation) {
    std::vector<std::vector<float>> outs;
    for (const auto& pt : knowns) {
      outs.push_back(permutation(pt[0], pt[1]));
    }
    return outs;
  };

  auto warp1 = [d1, d2](float x, float y) {
    return std::vector<float>({x - d1/2, y-d2/2});
  };
  auto warp2 = [d1, d2](float x, float y) {
    float r = sqrt(x*x + y*y);
    if (r == 0.0f) { r = .1f; }
    return std::vector<float>({x * 1.3f * abs(x / r), y * 1.6f * abs(y / r)});
  };

  std::vector<std::vector<float>> centered = apply(initial_known_points, warp1);
  std::vector<std::vector<float>> warped = apply(centered, warp2);

  // We should still have the same number of points.
  EXPECT_EQ(warped.size(), d1*d2);

  // Our sample points happen to be the centered point locations - just use them.
  std::vector<std::vector<float>> sample_points = centered;

	std::cout << "known points: " << std::endl;
	for (const auto& pt : warped) {
		std::cout << "<" << pt[0] << ", " << pt[1] << ">";
	}
	std::cout << std::endl;
	std::cout << "sample points: " << std::endl;
	for (const auto& pt : sample_points) {
		std::cout << "<" << pt[0] << ", " << pt[1] << ">";
	}
	std::cout << std::endl;

  for (const auto& pt : sample_points) {
		std::cout << "<" << pt[0] << ", " << pt[1] << ">: " << std::endl;
    auto soln = solver::FindSupport(warped, pt);
		for (const auto& p : soln) {
			std::cout << "index: " << p.first << " x " << p.second << std::endl;
		}
		if (soln.empty()) {
			std::cout << "No basis for pt. Must pad with boundary condition." << std::endl;
		}
  }

}

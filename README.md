# Halogen

An image pre-processing DSL for defining simple image input pipelines.

## BUILDING

This project uses [Bazel](https://bazel.build/) for its build system. To build
and run the targets bazel should be installed. It should then be possible to run
any of the tests or build any of the libraries by invoking the appropriate bazel
command such as:

```
bazel test //interpreter:interpreter_test
bazel build //use_cases:use_case1
```

To run the interpreter test.

## Project for UW CSE-501 Winter 2019.

## Milestone 3:

types/

    Contains all of the definitions for the language/ast/ir types.
    - ast2.h contains the AST definition used for this milestone.

interpreter/

    Contains a simple interpreter for running an image pipeline as well as some
    simple tests that run it on various (simplified) use cases.

use\_cases/

    Contains the AST for the 3 use cases described in milestone 2.

## Milestone 4:

solver/

    Contains logic for solving the mapping problems.

compiler/

    Contains logic for solving the mapping problems.


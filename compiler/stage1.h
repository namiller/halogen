#ifndef HALOGEN_COMPILER_STAGE1_H
#define HALOGEN_COMPILER_STAGE1_H

#include <vector>
#include <utility>
#include <set>

#include "types/ast1.h"
#include "types/ast2.h"

// Big idea:
// The output has a set of R^n sample points which represent the location of the
// output pixel is the n dimensional coordinate space. The input has a set of
// R^n sample points which represent the location of the input values for the
// input image. At any stage of our AST1 we can compute the coordinates of
// these input points, as well as the locations of the outputs. We can therefore
// determine the set of inputs that map to each output. We can also inject new
// serializations in the pipeline that represent reordering the inputs to
// simplify the future mappings.

namespace compiler {

// Validates that required properties hold for the given tree. These include:
//  - The dimensionality is consistent by index of map
//  - The dimensionality mappings map from the correct dimension for the
//    previous node.
bool CheckAst(const ast::Ast1& ast1);

// Collapses sequential operations in the Ast1 into single operations if
// desired.
ast::Ast1 Optimize(const ast::Ast1& ast1);

// Generates an Ast2 representing the same operations expressed in the given
// ast1.
ast::Ast2 Compile(const ast::Ast1& ast1);

// Computes the list of known coordinates (ie the i'th location is the location
// of the i'th pixel in the input byte buffer) for the given map. The map must
// map from dimension 1.
std::vector<ast::Flocation> KnownLocations(const ast::Ast1Expr& map,
                                           int input_size);

// Computes the set of weighted indicies into the original image that corespond
// to the output at each requested location - the i'th coordinate in the input
// coresponds to the i'th set in the return.
std::vector<std::set<std::pair<int, float>>> ComputeRawMap(
    const std::vector<ast::Flocation>& knowns,
    const std::vector<ast::Flocation>& requests);

// Computes the mapping for a single output location.
std::set<std::pair<int, float>> ComputeSingleMap(
    const std::vector<ast::Flocation>& knowns,
    const ast::Flocation& request);

// Condenses the given sequence of expressions into a single expression.
ast::Ast1Expr Collapse(const std::vector<ast::Ast1Expr>& epxressions);

// Determines the cost of converting the given expression into an Ast2
// expression such that the values at the given coordinates are preserved.
int ExpressionCost(const ast::Ast1Expr& expression);

}  // namespace compiler

#endif  // HALOGEN_COMPILER_STAGE1_H

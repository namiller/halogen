#include "compiler/stage1.h"

#include "solver/linear_combinations.h"

#include <vector>
#include <cassert>

namespace compiler {

std::vector<ast::Flocation> KnownLocations(const ast::Ast1Expr& map,
                                           int input_size) {
  assert(map.in_dim == 1);
  std::vector<ast::Flocation> results;
  for (int i = 0; i < input_size; i++) {
    results.emplace_back(map.map(std::vector<float>({float(i)})));
  }
  return results;
}

std::set<std::pair<int, float>> ComputeSingleMap(
    const std::vector<ast::Flocation>& knowns,
    const ast::Flocation& request) {

}

}  // namespace compiler

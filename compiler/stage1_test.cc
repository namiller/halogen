#include "compiler/stage1.h"

#include "gtest/gtest.h"

TEST(Stage1Test, KnownLocations) {
  ast::Ast1Expr map;
  map.in_dim = 1;
  map.out_dim = 3;
  map.map = [](ast::Flocation loc) -> ast::Flocation {
    int index = loc[0];
    return {float(index / 9), float((index / 3) % 3), float(index % 3)};
  };

  auto locs = compiler::KnownLocations(map, 27);
  ASSERT_EQ(locs.size(), 27);
  ASSERT_EQ(locs[0].size(), 3);
  EXPECT_EQ(locs[0][0], 0);
  EXPECT_EQ(locs[0][1], 0);
  EXPECT_EQ(locs[0][2], 0);
  ASSERT_EQ(locs[26].size(), 3);
  EXPECT_EQ(locs[26][0], 2);
  EXPECT_EQ(locs[26][1], 2);
  EXPECT_EQ(locs[26][2], 2);
}

TEST(Stage1Test, ExactMapping) {
  std::vector<ast::Flocation> knowns = { {0,0,0}, {1,1,1}, {1,2,3} };
  ast::Flocation wanted = {1,1,1};
  auto result = ComputeSingleMap(knowns, wanted);
  ASSERT_EQ(result.size(), 1);
  EXPECT_EQ(result.begin()->first, 1);
  EXPECT_EQ(result.begin()->second, 1.0);
}

TEST(Stage1Test, LinearRawMapping) {
  std::vector<ast::Flocation> knowns = { {0,0,0}, {1,1,1}, {1,2,3} };
  ast::Flocation wanted = {.25,.25,.25};
  auto result = ComputeSingleMap(knowns, wanted);
  ASSERT_EQ(result.size(), 2);
  auto it = resutl.begin();
  EXPECT_EQ(it->first, 0);
  EXPECT_EQ(it->second, 0.75);
  it++;
  EXPECT_EQ(it->first, 1);
  EXPECT_EQ(it->second, 0.25);
}

#ifndef HALOGEN_TYPES_AST1_H
#define HALOGEN_TYPES_AST1_H

#include <vector>
#include <utility>
#include <functional>

namespace ast {

// The location of a point in R^n
typedef std::vector<float> Flocation;
// The shape of a multi dimensional extent
typedef std::vector<int> Shape;
// A function that Maps from R^n -> R^m.
typedef std::function<Flocation(Flocation)> RnRmFunction;
// A floating point range (with optional stride.
struct Frange {
  float start;
  float stop;
  float stride = 1;
};

class Ast1Node {};

class Ast1Discretization : public Ast1Node {
 public:
  std::vector<Frange> domains;
};

class Ast1Expr : public Ast1Node {
 public:
  RnRmFunction map;
  int in_dim;
  int out_dim;
};

class Ast1Data : public Ast1Node { };

class Ast1Yieldable : public Ast1Node {
 public:
  Ast1Discretization* domain;
  Ast1Data* data;
};

// A sparse representation for an input data.
class Ast1Sparse : public Ast1Data {
 public:
  // maps real indexed locations to indexes into the backing buffer.
  std::vector<std::pair<Flocation, int>> values;
  int dimensionality;
};

class Ast1View : public Ast1Data {
 public:
  Ast1Data* source;
  Ast1Expr* expr;
  int dimensionality;
};

class Ast1 {
 public:
  Ast1(Ast1Node* root) : root(root) {}
  Ast1Node* root;
};

}  // namespace ast

#endif  // HALOGEN_TYPES_AST1_H

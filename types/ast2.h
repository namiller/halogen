#ifndef HALOGEN_TYPES_AST_2_H
#define HALOGEN_TYPES_AST_2_H

#include <stddef.h>
#include <vector>
#include <set>
#include <utility>
#include <functional>

namespace ast {

typedef std::function<std::set<std::pair<int, float>>(int)> WeightedMap;

WeightedMap ReorderMap(std::function<int(int)> map) {
  return WeightedMap([map](int index) {
    std::set<std::pair<int, float>> values;
    values.insert(std::make_pair(map(index), 1.0f));
    return values;
  });
}

typedef int NodeType;

const NodeType kTypeBits = 9;
// Abstract super class for all nodes for the AST.
class Ast2Node {
 public:
  virtual NodeType type() const = 0;
  virtual bool fully_specified() = 0;
  virtual std::vector<const Ast2Node*> children() const = 0;
  // Introspection - true if the Ast2Node is of the given type.
  bool is_type(int concrete_type) const {
    return (type() & ((1 << (kTypeBits + 1)) - 1)) == concrete_type;
  }
  // Introspection - true if the Ast2Node inherits from the given type.
  bool of_type(NodeType type_mask) const {
    return type() & type_mask;
  }
};

// Abstract node representing a block of real data.
class Ast2Data : public Ast2Node {
 public:
  static const NodeType kType = 1 << (kTypeBits + 1);
  virtual int data_type() const = 0;
  NodeType type() const override {
    return data_type() | kType;
  }
};

// Abstract node representing a constant.
class Ast2Constant : public Ast2Node {
 public:
  static const NodeType kType = 1 << (kTypeBits + 2);
  virtual int constant_type() const = 0;
  NodeType type() const override {
    return constant_type() | kType;
  }
};

// Node representing a discretized expression.
class Ast2Expr : public Ast2Node {
 public:
  static const NodeType kType = 2;
  virtual NodeType type() const { return kType; }
  std::vector<const Ast2Node*> children() const { return {}; }
  bool fully_specified() override {
    return bool(map);
  }
  // Mapping from destination index, to the set of source indicies with their
  // respective weights.
  WeightedMap map;
};

// Node representing a block of memory.
class Ast2Buffer : public Ast2Data {
 public:
  static const NodeType kType = 3;
  int data_type() const override { return kType; }
  std::vector<const Ast2Node*> children() const { return {}; }
  bool fully_specified() override {
    return !name.empty() && size != 0;
  }
  size_t size;
  std::string name;
};

// Node representing a view into a block of memory.
class Ast2View : public Ast2Data {
 public:
  static const NodeType kType = 4;
  int data_type() const override { return kType; }
  bool fully_specified() override {
    return expr != nullptr && source != nullptr;
  }
  std::vector<const Ast2Node*> children() const { return {expr, source}; }
  Ast2Expr* expr;
  Ast2Data* source;
};

// Node representing an output from the program.
class Ast2Output : public Ast2Node {
 public:
  static const NodeType kType = 5;
  virtual NodeType type() const { return kType; }
  bool fully_specified() override {
    return source != nullptr;
  }
  std::vector<const Ast2Node*> children() const { return {source}; }
  Ast2Data* source;
  std::vector<int> indexes;
};

class Ast2 {
 public:
  Ast2(Ast2Node* root) : root(root) {}
  Ast2Node* root;

  std::vector<const Ast2Node*> GetAll(NodeType type) const;
};

}  // namespace ast.

#endif  // HALOGEN_TYPE_AST_2_H

#include "types/ast2.h"

#include "gtest/gtest.h"

TEST(AstTest, TypeTraits) {

  ast::Ast2Node *node;
  {
    ast::Ast2Buffer buffer;
    node = &buffer;
    EXPECT_TRUE(node->is_type(ast::Ast2Buffer::kType));
    EXPECT_FALSE(node->is_type(ast::Ast2View::kType));
    EXPECT_TRUE(node->of_type(ast::Ast2Data::kType));
  }
  {
    ast::Ast2View view;
    node = &view;
    EXPECT_FALSE(node->is_type(ast::Ast2Buffer::kType));
    EXPECT_TRUE(node->is_type(ast::Ast2View::kType));
    EXPECT_TRUE(node->of_type(ast::Ast2Data::kType));
  }
}


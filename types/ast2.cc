#include "ast2.h"
#include <queue>

namespace ast {

std::vector<const Ast2Node*> Ast2::GetAll(NodeType type) const {
  std::vector<const Ast2Node*> results;
  std::queue<const Ast2Node*> nodes;
  nodes.push(root);
  while (!nodes.empty()) {
    const auto* node = nodes.front();
    nodes.pop();
    if (node->is_type(type)) {
      results.push_back(node);
    }
    for (const auto* child : node->children()) {
      nodes.push(child);
    }
  }
  return results;
}

}  // namespace ast.

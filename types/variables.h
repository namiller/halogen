#include <memory>
#include <set>
#include <map>
#include "absl/memory/memory.h"

// Class definitions related to variable types.

// Super class that defines common interface for all variables.
class Variable {};

// A free variable (symbolic variable). These variables are abstract variables
// which will have their value inferred or set explicitly by runtime.
class FreeVariable : public Variable {
 private:
  std::string name_;

 public:
  FreeVariable(std::string name) : name_(name) {}
};

// A dimension variable is a variable that indexes into the dimensions of a
// coordinate space. For example standard dimension variables might be x, y, z.
class DimensionVariable : public Variable {
 public:
  // Returns the index of the coordinate this variable represents.
  int index();
};

class DimensionFactorySingleton {
 private:
  std::map<int, DimensionVariable> vars;
  static std::unique_ptr<DimensionFactorySingleton> instance_;

 public:
  static DimensionFactorySingleton* instance() {
    if (!instance_) {
      instance_ = absl::make_unique<DimensionFactorySingleton>();
    }
    return instance_.get();
  }

  const DimensionVariable& get(int i) { return vars[i]; }
};

std::unique_ptr<DimensionFactorySingleton>
    DimensionFactorySingleton::instance_ = nullptr;

// Somehow the scope of this needs to be bounded.
class FreeVariableFactorySingleton {
 private:
  std::set<std::string> vars;
  static std::unique_ptr<FreeVariableFactorySingleton> instance_;

 public:
  static FreeVariableFactorySingleton* instance() {
    if (!instance_) {
      instance_ = absl::make_unique<FreeVariableFactorySingleton>();
    }
    return instance_.get();
  }

  FreeVariable get(std::string name) {}
};

namespace dvar {
// This is the only way D Variables should be created/accessed.
const DimensionVariable& dim(int i) {
  return DimensionFactorySingleton::instance()->get(i);
}
};  // namespace dvar

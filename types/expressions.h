#include <math.h>
#include <functional>
#include <iostream>
#include <map>
#include <set>

class var {
 private:
  std::string name_;

 public:
  var(std::string name) : name_(name) {}
  bool operator<(const var& rhs) const { return name_ < rhs.name_; }
};

class expr {
 public:
  std::function<int(std::map<var, int>)> impl_;
  std::set<var> free_variables_;

 public:
  expr(std::function<int(std::map<var, int>)> impl) : impl_(impl) {}
  expr(var v) : impl_([v](std::map<var, int> env) { return env[v]; }) {}
  expr(int c) : impl_([c](std::map<var, int> env) { return c; }) {}
  int operator()(std::map<var, int> env) const { return impl_(env); }
};

int isqrt(int x) { return int(sqrt(x)); }

#define REGISTER_EXPR_FUNC1(fctn)                                          \
  expr fctn(expr arg1) {                                                   \
    return expr(                                                           \
        [arg1](std::map<var, int> env) { return fctn(arg1.impl_(env)); }); \
  }

#define REGISTER_EXPR_OP(op)                         \
  expr operator op(expr lhs, expr rhs) {             \
    return expr([lhs, rhs](std::map<var, int> env) { \
      return lhs.impl_(env) op rhs.impl_(env);       \
    });                                              \
  }

REGISTER_EXPR_FUNC1(isqrt);
REGISTER_EXPR_OP(+);
REGISTER_EXPR_OP(-);
REGISTER_EXPR_OP(*);
REGISTER_EXPR_OP(/);
REGISTER_EXPR_OP(%);

#include "types/expressions.h"
#include "gtest/gtest.h"

TEST(ExpressionTest, TestRun) {
  var x("x");
  expr meta = isqrt(2 * x + 1);
  EXPECT_EQ(meta({{x, 3}}), 2);
  EXPECT_EQ(meta({{x, 4}}), 3);
}

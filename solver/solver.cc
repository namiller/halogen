#include "solver/solver.h"

#include <vector>
#include <map>
#include <iostream>

#include "solver/linear_combinations.h"
#include "solver/pt_selector.h"

namespace solver {

std::map<int, float> FindSupport(
		const std::vector<std::vector<float>>& knowns,
		const std::vector<float>& unknown) {
	int dim = unknown.size();
	std::map<int, float> soln;
	std::vector<int> reindex;
	// dim * 2 + 1 is the triangulation necessary plus a redundant addition of dim for safety.
	for (int k = 1; k < dim * 2 + 1; k++) {
		auto subset_ind = FindNClosest(unknown, knowns, k);
		std::vector<std::vector<float>> subset(subset_ind.size());
		for (int i = 0; i < subset_ind.size(); i++) {
			subset[i] = knowns[subset_ind[i]];
		}
		soln = min_projection(subset, unknown, std::min(k, dim + 1), .000001);
		if (!soln.empty()) {
			// Remap the subset index back to the full index.
			std::map<int, float> ret;
			for (const auto& p : soln) {
				ret[subset_ind[p.first]] = p.second;
			}
			return ret;
		}
	}
	return std::map<int, float>();
}

}  // namespace solver

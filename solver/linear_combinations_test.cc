#include "linear_combinations.h"

#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include "gtest/gtest.h"
#include "z3++.h"

TEST(LinearCombinationsTest, Solvable) {
  std::vector<std::vector<float>> basis = {{0, 0, 1}, {1, 5, 3}, {0, 0, 5}};
  std::vector<float> target = {0, 0, 3};

  auto soln = solver::min_projection(basis, target, 2, .00001);
  EXPECT_EQ(soln.size(), 2);
  EXPECT_LE(std::abs(soln[0] - .5), .0001);
  EXPECT_LE(std::abs(soln[2] - .5), .0001);
}

TEST(LinearCombinationsTest, ImpossibleSquare) {
	std::vector<std::vector<float>> basis = {{0, 0},
																					 {0, 1},
																					 {1, 0},
																					 {1, 1}};
	std::vector<float> target = {-.5, .5};
	auto soln = solver::min_projection(basis, target, 4, .000001);
	EXPECT_EQ(soln.size(), 0);
}

TEST(LinearCombinationsTest, Square) {
	std::vector<std::vector<float>> basis = {{0, 0},
																					 {0, 1},
																					 {1, 0},
																					 {1, 1}};
	std::vector<float> target = {.25, .5};
	auto soln = solver::min_projection(basis, target, 4, .000001);
	EXPECT_NE(soln.size(), 0);
}

TEST(LinearCombinationsTest, Triplet) {
	std::vector<std::vector<float>> basis = {{-3.67695, -4.52548},
																					 {-5.07565, -3.99805},
																					 {-4.59619, -5.65685}};
	std::vector<float> target = {-5, -5};
	try {
	auto soln = solver::min_projection(basis, target, 3, .000001);
	} catch (z3::exception ex) {
		std::cout << ex << std::endl;
	}
//	EXPECT_EQ(soln.size(), 3);
}


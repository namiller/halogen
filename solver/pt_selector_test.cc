#include "pt_selector.h"

#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include "gtest/gtest.h"

TEST(PtSelectorTest, Closest) {
  std::vector<std::vector<float>> pts = {{0, 0, 1}, {1, 5, 3}, {0, 0, 5}};
  std::vector<float> pt = {0, 0, 2};

  auto soln = solver::FindNClosest(pt, pts, 1);
  EXPECT_EQ(soln.size(), 1);
	EXPECT_EQ(soln[0], 0);
}

TEST(PtSelectorTest, Closest2) {
  std::vector<std::vector<float>> pts = {{0, 0, 1}, {1, 5, 3}, {1, 2, 2}};
  std::vector<float> pt = {0, 0, 2};

  auto soln = solver::FindNClosest(pt, pts, 2);
  EXPECT_EQ(soln.size(), 2);
	EXPECT_EQ(soln[0], 2);
	EXPECT_EQ(soln[1], 0);
}


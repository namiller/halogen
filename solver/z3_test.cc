
#include "z3++.h"
#include "gtest/gtest.h"

#include <string>
#include <iostream>
#include <vector>

using namespace z3;

TEST(Z3Test, RealToFloat) {
	context c;
	solver s(c);

	expr x = c.real_const("x");
	expr y = c.real_val("2.5");
	s.add(x == y / c.real_val("10.5"));
	EXPECT_EQ(s.check(), sat);
	model m = s.get_model();
	std::cout << m.eval(x) << std::endl;
	float f  = std::stof(m.eval(x).get_decimal_string(10));
	std::cout << f << std::endl;
}

TEST(Z3Test, RealDecimal) {
	context c;
	solver s(c);

	expr x = c.real_const("x");
	expr y = c.real_val("1.345234");
	expr z = x + (y * x);
	s.add(z == c.real_val("9.12312"));
	EXPECT_EQ(s.check(), sat);
	model m = s.get_model();
	std::cout << m.eval(x) << std::endl;
}
/*
TEST(Z3Test, SolnFindableFpa) {
	context c;
	solver s(c);

	expr w1 = c.fpa_const("w1", 8, 24);
	expr w2 = c.fpa_const("w2", 8, 24);
	expr w3 = c.fpa_const("w3", 8, 24);
	std::cout << "HERE" << std::endl;

	expr v11 = c.fpa_val(-3.67695f);
	expr v12 = c.fpa_val(-4.52548f);
	expr v21 = c.fpa_val(-5.07565f);
	expr v22 = c.fpa_val(-3.99805f);
	expr v31 = c.fpa_val(-4.59619f);
	expr v32 = c.fpa_val(-5.65685f);
	std::cout << "HERE" << std::endl;

	s.add(c.fpa_val(.999999f) <= w1 + w2 + w3);
	std::cout << "HERE" << std::endl;
	s.add(w1 + w2 + w3 <= c.fpa_val(1.000001f));
	std::cout << "HERE" << std::endl;


	s.add(w1 * v11 + w2 * v21 + w3 * v31 <= c.fpa_val(-4.4999999f));
	s.add(c.fpa_val(-4.5000001f) <= w1 * v11 + w2 * v21 + w3 * v31);
	s.add(w1 * v12 + w2 * v22 + w3 * v32 <= c.fpa_val(-4.6999999f));
	s.add(c.fpa_val(-4.7000001f) <= w1 * v12 + w2 * v22 + w3 * v32);

	std::cout << "HERE" << std::endl;
	EXPECT_EQ(s.check(), sat);
	std::cout << "HERE" << std::endl;
	model m = s.get_model();
	std::cout << m.eval(w1) << std::endl;
	std::cout << m.eval(w2) << std::endl;
	std::cout << m.eval(w3) << std::endl;
}
*/
TEST(Z3Test, SolnFindableReals) {
	context c;
	solver s(c);

	expr w1 = c.real_const("w1");
	expr w2 = c.real_const("w2");
	expr w3 = c.real_const("w3");
	std::cout << "HERE" << std::endl;

	expr v11 = c.real_val("-3.67695");
	expr v12 = c.real_val("-4.52548");
	expr v21 = c.real_val("-5.07565");
	expr v22 = c.real_val("-3.99805");
	expr v31 = c.real_val("-4.59619");
	expr v32 = c.real_val("-5.65685");
	std::cout << "HERE" << std::endl;

	s.add(c.real_val(".999999") <= w1 + w2 + w3);
	std::cout << "HERE" << std::endl;
	s.add(w1 + w2 + w3 <= c.real_val("1.000001"));
	std::cout << "HERE" << std::endl;

	s.add(w1 * v11 + w2 * v21 + w3 * v31 <= c.real_val("-4.4999999"));
	s.add(c.real_val("-4.5000001") <= w1 * v11 + w2 * v21 + w3 * v31);
	s.add(w1 * v12 + w2 * v22 + w3 * v32 <= c.real_val("-4.6999999"));
	s.add(c.real_val("-4.7000001") <= w1 * v12 + w2 * v22 + w3 * v32);

	std::cout << "HERE" << std::endl;
	EXPECT_EQ(s.check(), sat);
	std::cout << "HERE" << std::endl;
	model m = s.get_model();
	std::cout << m.eval(w1) << std::endl;
	std::cout << m.eval(w2) << std::endl;
	std::cout << m.eval(w3) << std::endl;
}

// Ensure that constraints are still applied even if the object is destroyed.
TEST(Z3Test, ScopedConstraint) {
  context c;
  solver s(c);

  expr x = c.int_const("x");
  {
    expr y = x + 5;
    s.add(y == 3);
  }
  {
    expr z = x + 3;
    s.add(z == 100);
  }
  EXPECT_EQ(s.check(), unsat);
}

// Ensure that even large statements work.
TEST(Z3Test, LargeStatement) {
  int size = 10000;
  context c;
  solver s(c);

  std::vector<expr> expressions;
  expressions.push_back(c.int_val(0));
  for (int i = 1; i < size; i++) {
    expressions.push_back(c.int_const(std::to_string(i).c_str()));
    s.add(expressions[i] == expressions[i - 1] + 1);
  }
  EXPECT_EQ(s.check(), sat);
  model m = s.get_model();
  EXPECT_EQ(m.eval(expressions.back()), size);
}

// Ensure that "recursive" definitions don't actually recurse.
TEST(Z3Test, IterativeConstruction) {
  context c;

  expr x = c.int_const("x");
  expr x4 = c.int_val(1);
  for (int i = 0; i < 4; i++) {
    x4 = x4 * x;
  }
  solver s(c);
  s.add(x4 == 16);
  EXPECT_EQ(s.check(), sat);
  model m = s.get_model();
	EXPECT_EQ(m.eval(x4).to_string(), "16");
	EXPECT_NE(m.eval(x4).to_string(), "17");
  EXPECT_TRUE(m.eval(x).to_string() == "2" || m.eval(x).to_string() == "(- 2)");
}

// Ensure that the inline boolean operator is ok.
TEST(Z3Test, InlineBoolean) {
  context c;
  expr x = c.int_const("x");
  expr y = x < 3 ? c.int_val(3) : c.int_val(9);
  solver s(c);
  s.add(x == 10);
  EXPECT_EQ(s.check(), sat);
  model m = s.get_model();
  EXPECT_EQ(m.eval(y), 9);
}


// Taken from:
// https://github.com/Z3Prover/z3/blob/master/examples/c%2B%2B/example.cpp
TEST(Z3Test, DeMorgan) {
  context c;

  expr x = c.bool_const("x");
  expr y = c.bool_const("y");
  expr conjecture = (!(x && y)) == (!x || !y);

  solver s(c);
  // adding the negation of the conjecture as a constraint.
  s.add(!conjecture);
  EXPECT_EQ(s.check(), unsat);
}

TEST(Z3Test, FindModel) {
  context c;
  solver s(c);
  expr x = c.int_const("x");
  expr y = c.int_const("y");
  //solver s(c);

  s.add(x >= 1);
  s.add(y < x + 3);

  EXPECT_EQ(s.check(), sat);

  model m = s.get_model();

  // traversing the model
  for (unsigned i = 0; i < m.size(); i++) {
    func_decl v = m[i];
    // this problem contains only constants
    assert(v.arity() == 0);
  }
  // we can evaluate expressions in the model.
  EXPECT_EQ(m.eval(x + y + 1), 5);
  EXPECT_NE(m.eval(x + y + 1), 6);
}

TEST(Z3Test, Proof) {
  context c;
  expr x      = c.int_const("x");
  expr y      = c.int_const("y");
  sort I      = c.int_sort();
  func_decl g = function("g", I, I);

  solver s(c);
  expr conjecture1 = implies(x == y, g(x) == g(y));
  s.add(!conjecture1);
  EXPECT_EQ(s.check(), unsat);

  s.reset(); // remove all assertions from solver s

  expr conjecture2 = implies(x == y, g(g(x)) == g(y));
  s.add(!conjecture2);
  EXPECT_EQ(s.check(), sat);
  model m = s.get_model();
  std::cout << "counterexample:\n" << m << "\n";
  std::cout << "g(g(x)) = " << m.eval(g(g(x))) << "\n";
  std::cout << "g(y)    = " << m.eval(g(y)) << "\n";
}


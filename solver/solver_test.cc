#include "solver.h"

#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include "gtest/gtest.h"

TEST(SolverTest, Solvable) {
  std::vector<std::vector<float>> pts = {{0, 0, 1}, {1, 5, 3}, {0, 0, 5}};
  std::vector<float> pt = {0, 0, 2};

  auto soln = solver::FindSupport(pts, pt);
	EXPECT_EQ(soln.size(), 2);
	EXPECT_LE(std::abs(soln[0] - .75), .00001);
	EXPECT_LE(std::abs(soln[2] - .25), .00001);
}


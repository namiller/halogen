#ifndef HALOGEN_SOLVER_SOLVER_H
#define HALOGEN_SOLVER_SOLVER_H

#include <vector>
#include <map>

namespace solver {

std::map<int, float> FindSupport(
		const std::vector<std::vector<float>>& knowns,
		const std::vector<float>& unknown);

}

#endif  // HALOGEN_SOLVER_SOLVER_H

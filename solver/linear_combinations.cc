#include "solver/linear_combinations.h"

#include <iostream>
#include <cassert>
#include <set>
#include <utility>
#include <array>
#include <vector>

#include "absl/strings/str_format.h"
#include "absl/strings/str_split.h"

#include "z3++.h"

namespace solver {

namespace {
z3::expr make(z3::context& c, float f) {
	return c.real_val(absl::StrFormat("%.7f", f).c_str());
}

// Beginning to hate z3 in cpp...
float to_float(z3::expr e) {
	return std::stof(e.get_decimal_string(10));
}

} // namespace

// TODO(namiller): We probably want to find the solution from points nearest to
// pt.

// basis is an NxD matrix representing the N vectors of dimension D for which
// values are known.
//
// pt is a 1xD vector representing the desired location
//
// w is a 1xN vector representing the weights applied to M such that:
// w * basis = pt
// and the L0 norm of w is scale, and the sum of elements in w is 1.
//
// The value returned is a sparse representation of w (indexes and values of
// non-zeros).
//
// NOTE: This is equivalent to determining a set in basis of scale points in D
// dimensions that contain point pt within their convex hull (and the
// barycentric coordinates of pt within the polytope).
std::map<int, float> min_projection(
    const std::vector<std::vector<float>>& basis,
    const std::vector<float>& pt, int scale, float tolerance) {
  z3::context c;
  z3::solver s(c);
  //z3::optimize s(c);

  // Check that basis is consistent.
  assert(!basis.empty());
  auto dims = pt.size();
  for (const auto& base : basis) {
    assert(dims == base.size());
  }

  // The weights to apply to each vector.
  std::vector<z3::expr> weights;
  // The weights must add up to 1, and have an l0 norm of scale.
  z3::expr weight_sum = make(c, 0.0f);
  z3::expr l0_norm = c.int_val(0);
  for (size_t i = 0; i < basis.size(); i++) {
    auto weight = c.real_const(absl::StrFormat("w_%i", i).c_str());
		s.add(make(c,0.0f) <= weight);
    weight_sum = weight_sum + weight;
    l0_norm = l0_norm + (weight == make(c, 0.0f) ? c.int_val(0) : c.int_val(1));
    weights.emplace_back(std::move(weight));
  }
  s.add(l0_norm <= scale);
  s.add(make(c, 1.0f - tolerance) <= weight_sum);
  s.add(weight_sum <= make(c, 1.0f + tolerance));

  // The weighted sum must add up to out target point.
  for (size_t d = 0; d < dims; d++) {
    z3::expr computed_value = make(c, 0.0f);
    for (size_t b = 0; b < basis.size(); b++) {
      computed_value = computed_value + (weights[b] * make(c, basis[b][d]));
    }
    s.add(computed_value <= make(c, pt[d] + tolerance));
    s.add(make(c, pt[d] - tolerance) <= computed_value);
  }

  /*z3::expr total_cost = c.fpa_val(0.0f);
  for (size_t b = 0; b < basis.size(); b++) {
    z3::expr cost = c.fpa_val(0.0f);
    for (size_t d = 0; d < dims; d++) {
      auto x = c.fpa_val(basis[b][d]) - c.fpa_val(pt[d]);
      cost = cost + (x * x);
    }
    total_cost = total_cost + ((weights[b] == c.fpa_val(0.0f)) ? c.fpa_val(0.0f) : cost);
  }*/

  //s.minimize(total_cost);

  if (s.check() != z3::sat) {
    return std::map<int, float>();
  }
  z3::model m = s.get_model();
  std::map<int, float> ret;
  for (size_t i = 0; i < basis.size(); i++) {
    float v = to_float(m.eval(weights[i]));
    if (v != 0) {
      ret[i] = v;
    }
  }
  return ret;
}


}  // namespace solver


#ifndef HALOGEN_SOLVER_LINEAR_COMBINATIONS_H
#define HALOGEN_SOLVER_LINEAR_COMBINATIONS_H

#include <map>
#include <vector>

namespace solver {

// TODO(namiller): Refactor this to optimize for the case of repeated basis
// queried for many vectors.

// TODO(namiller): Make the caller of this perform some basis optimization to
// only search within a near neighborhood (perhaps an expanding neighboorhood).

// Returns the minimum projection of the input vector onto the basis set
// provided.
std::map<int, float> min_projection(
    const std::vector<std::vector<float>>& basis,
    const std::vector<float>& pt, int scale, float tolerance);


}  // namespace solver

#endif  // HALOGEN_SOLVER_LINEAR_COMBINATIONS_H

#ifndef HALOGEN_SOLVER_PT_SELECTOR_H
#define HALOGEN_SOLVER_PT_SELECTOR_H

#include <vector>
#include <cstddef>

// Algorithm for selecting the correct set of points from which to find the
// linear combination.
namespace solver {

typedef std::vector<float> vec;

// Finds the indexes in pts of the count closest points to pt.
std::vector<size_t> FindNClosest(const vec& pt, const std::vector<vec> pts, size_t count);

}  // namespace solver

#endif  // HALOGEN_SOLVER_PT_SELECTOR_H

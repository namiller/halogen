#include "solver/pt_selector.h"

#include <cmath>
#include <vector>
#include <queue>
#include "nanoflann.hpp"

// Algorithm for selecting the correct set of points from which to find the
// linear combination.
namespace solver {

float L2(const vec& v1, const vec& v2) {
	if (v1.size() != v2.size()) {
		return -1;
	}
	float sq_sum = 0;
	for (size_t i = 0; i < v1.size(); i++) {
		float dist = v1[i] - v2[i];
		sq_sum += dist * dist;
	}
	return std::sqrt(sq_sum);
}

std::vector<size_t> FindNClosest(const vec& pt, const std::vector<vec> pts, size_t count) {
	std::vector<size_t> soln;
	auto nearest = [&pt, &pts](size_t i1, size_t i2) {
		return L2(pts[i1], pt) < L2(pts[i2], pt);
	};
	std::priority_queue<size_t, std::vector<size_t>, decltype(nearest)> q(nearest);

	for (size_t i = 0; i < pts.size(); i++) {
		q.push(i);
		if (q.size() > count) {
			q.pop();
		}
	}

	while (!q.empty()) {
		soln.push_back(q.top());
		q.pop();
	}

	return soln;
}

}  // namespace solver

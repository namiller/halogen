#ifndef HALOGEN_TYPES_INTERPRETER_H
#define HALOGEN_TYPES_INTERPRETER_H

#include <stddef.h>
#include <stdint.h>
#include <map>

#include "types/ast2.h"

namespace interpreter {

// TODO(namiller): Add bounds checking.
class ByteBuffer {
 public:
  ByteBuffer();
  ByteBuffer(const uint8_t* bytes, size_t size);
  ByteBuffer(size_t capacity);
  // No copying.
  ByteBuffer(const ByteBuffer&) = delete;
  ByteBuffer& operator=(const ByteBuffer&) = delete;
  // Moving is ok.
  ByteBuffer& operator=(ByteBuffer&&);
  ByteBuffer(ByteBuffer&& other);
  ~ByteBuffer();

  uint8_t* bytes() { return data_; }
  const uint8_t* bytes() const { return data_; }
  size_t size() const { return size_; }
 private:
  size_t size_;
  uint8_t* data_;
};

typedef std::map<const ast::Ast2Buffer*, ByteBuffer> EvaluationContext;
typedef std::map<const ast::Ast2Output*, ByteBuffer> EvaluationResult;

EvaluationResult evaluate(const ast::Ast2& ast, const EvaluationContext& context);

}  // namespace interpeter.

#endif  // HALOGEN_TYPES_INTERPRETER_H

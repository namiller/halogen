#include "interpreter/interpreter.h"

#include <functional>
#include <set>
#include <utility>

#include "types/ast2.h"
#include "gtest/gtest.h"


TEST(InterpreterTest, Reorder) {
  int size = 100;
  ast::Ast2Buffer input;
  ast::Ast2Expr reorder;
  reorder.map = ast::WeightedMap([size] (int i) {
    std::set<std::pair<int, float>> values;
    values.insert(std::make_pair(i / 2 + (i % 2) * (size / 2), 1.0f));
    return values;
  });
  ast::Ast2View new_order;
  new_order.expr = &reorder;
  new_order.source = &input;

  ast::Ast2Output output;
  output.source = &new_order;
  output.indexes = {0, 11, 22, 33, 44, 55, 66, 77, 88, 99};

  interpreter::ByteBuffer input_buffer(size);
  for (int i = 0; i < size; i++) {
    input_buffer.bytes()[i] = i;
  }

  interpreter::EvaluationContext context;
  context.emplace(&input, std::move(input_buffer));
  auto results = interpreter::evaluate(&output, context);
  ASSERT_NE(results.find(&output), results.end());

  auto output_buffer = std::move(results[&output]);

  EXPECT_EQ(output_buffer.size(), size_t(10));
  EXPECT_EQ(output_buffer.bytes()[0], 0);
  EXPECT_EQ(output_buffer.bytes()[1], 55);
  EXPECT_EQ(output_buffer.bytes()[2], 11);
  EXPECT_EQ(output_buffer.bytes()[3], 66);
  EXPECT_EQ(output_buffer.bytes()[4], 22);
  EXPECT_EQ(output_buffer.bytes()[5], 77);
  EXPECT_EQ(output_buffer.bytes()[6], 33);
  EXPECT_EQ(output_buffer.bytes()[7], 88);
  EXPECT_EQ(output_buffer.bytes()[8], 44);
  EXPECT_EQ(output_buffer.bytes()[9], 99);
}

TEST(IntepreterTest, DoubleReflect) {
  const int size = 100;
  std::vector<int> values(size);
  for (int i = 0; i < size; i++) {
    values[i] = i;
  }
  ast::Ast2Buffer input;
  ast::Ast2Expr reflect;
  reflect.map = ast::ReorderMap([size] (int i) {
    return size - i - 1;
  });
  ast::Ast2View intermediate;
  intermediate.expr = &reflect;
  intermediate.source = &input;
  ast::Ast2View result;
  result.expr = &reflect;
  result.source = &intermediate;
  ast::Ast2Output output;
  output.source = &result;
  output.indexes = values;

  interpreter::ByteBuffer input_buffer(size);
  for (int i = 0; i < size; i++) {
    input_buffer.bytes()[i] = i;
  }

  interpreter::EvaluationContext context;
  context.emplace(&input, std::move(input_buffer));
  auto results = interpreter::evaluate(&output, context);
  ASSERT_NE(results.find(&output), results.end());

  auto output_buffer = std::move(results[&output]);

  EXPECT_EQ(output_buffer.size(), size_t(size));
  for (int i = 0; i < size; i++) {
    EXPECT_EQ(output_buffer.bytes()[i], i);
  }
}

TEST(InterpreterTest, Upsample) {
  int size = 100;
  ast::WeightedMap upsample([size](int index) {
    std::set<std::pair<int, float>> values;
    if (index % 2 && (index / 2) + 1 < size) {
      values.insert(std::make_pair(index / 2, 0.5f));
      values.insert(std::make_pair((index / 2) + 1, 0.5f));
    } else {
      values.insert(std::make_pair(index / 2, 1.0f));
    }
    return values;
  });

  ast::Ast2Buffer input;
  ast::Ast2Expr upsampler;
  upsampler.map = upsample;
  ast::Ast2View upsampled;
  upsampled.expr = &upsampler;
  upsampled.source = &input;

  ast::Ast2Output output;
  output.source = &upsampled;
  output.indexes = {0, 1, 10, 98, 99, 100, 101, 199};

  interpreter::ByteBuffer input_buffer(size);
  for (int i = 0; i < size; i++) {
    input_buffer.bytes()[i] = (i / 10) * 10;
  }

  interpreter::EvaluationContext context;
  context.emplace(&input, std::move(input_buffer));
  auto results = interpreter::evaluate(&output, context);
  ASSERT_NE(results.find(&output), results.end());

  auto output_buffer = std::move(results[&output]);

  EXPECT_EQ(output_buffer.size(), size_t(8));
  EXPECT_EQ(output_buffer.bytes()[0], 0);
  EXPECT_EQ(output_buffer.bytes()[1], 0);
  EXPECT_EQ(output_buffer.bytes()[2], 0);
  EXPECT_EQ(output_buffer.bytes()[3], 40);
  EXPECT_EQ(output_buffer.bytes()[4], 45);
  EXPECT_EQ(output_buffer.bytes()[5], 50);
  EXPECT_EQ(output_buffer.bytes()[6], 50);
  EXPECT_EQ(output_buffer.bytes()[7], 90);
}

#include "interpreter.h"

#include <string.h>
#include <stdint.h>
#include "types/ast2.h"
#include <iostream>

namespace interpreter {

ByteBuffer::ByteBuffer() : size_(0), data_(nullptr) { }

ByteBuffer::ByteBuffer(size_t capacity) : size_(capacity) {
  data_ = new uint8_t[capacity];
}

ByteBuffer::ByteBuffer(const uint8_t* bytes, size_t size) : ByteBuffer(size) {
  memcpy(data_, bytes, size);
}

ByteBuffer& ByteBuffer::operator=(ByteBuffer&& other) {
  data_ = other.data_;
  size_ = other.size_;
  other.data_ = nullptr;
  other.size_ = 0;
  return *this;
}

ByteBuffer::ByteBuffer(ByteBuffer&& other){
  *this = std::move(other);
}

ByteBuffer::~ByteBuffer() {
  if (size_ && data_) {
    delete[] data_;
  }
}


uint8_t evaluate(const ast::Ast2Buffer* node, int index,
                 const EvaluationContext& context);

uint8_t evaluate(const ast::Ast2View* node, int index,
                 const EvaluationContext& context);

uint8_t evaluate(const ast::Ast2Data* node, int index,
                 const EvaluationContext& context);

ByteBuffer evaluate(const ast::Ast2Output* output,
                    const EvaluationContext& context);

EvaluationResult evaluate(const ast::Ast2& tree,
                          const EvaluationContext& context);

uint8_t evaluate(const ast::Ast2Buffer* node, int index,
               const EvaluationContext& context) {
  return context.find(node)->second.bytes()[index];
}

uint8_t evaluate(const ast::Ast2View* node, int index,
               const EvaluationContext& context) {
  float acc = 0;
  for (const auto& sources : node->expr->map(index)) {
    acc += sources.second * evaluate(node->source, sources.first, context);
  }
  return acc;
}

uint8_t evaluate(const ast::Ast2Data* node, int index,
               const EvaluationContext& context) {
  if (node->is_type(ast::Ast2Buffer::kType)) {
    return evaluate(dynamic_cast<const ast::Ast2Buffer*>(node), index, context);
  } else if (node->is_type(ast::Ast2View::kType)) {
    return evaluate(dynamic_cast<const ast::Ast2View*>(node), index, context);
  }
  std::cerr << "Illegal node.";
  return 0;
}

ByteBuffer evaluate(const ast::Ast2Output* output,
                    const EvaluationContext& context) {
  ByteBuffer buffer(output->indexes.size());
  uint8_t* pos = buffer.bytes();

  for (int index : output->indexes) {
    *pos = evaluate(output->source, index, context);
    pos++;
  }
  return buffer;
}

EvaluationResult evaluate(const ast::Ast2& tree,
                          const EvaluationContext& context) {
  auto outputs = tree.GetAll(ast::Ast2Output::kType);
  EvaluationResult results;
  for (const auto* untyped_output : outputs) {
    const ast::Ast2Output* output = dynamic_cast<const ast::Ast2Output*>(untyped_output);
    results[output] = std::move(evaluate(output, context));
  }
  return results;
}

}  // namespace interpeter.
